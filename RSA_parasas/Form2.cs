﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace RSA_parasas
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Form1 f = new Form1();

            richTextBox4.Text = f.getzinute();
           
            button4.Enabled = false;
        }

        public MySqlConnection ConnectToDB()
        {
            string connection = @"server=localhost;uid=root;password=; Persist Security Info=True;database=rsa";
            MySqlConnection mySqlConnection = new MySqlConnection(connection);
            mySqlConnection.Open();
            return mySqlConnection;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string text;
            text = richTextBox4.Text;

            if (text != "")
            {
                MySqlConnection mySqlConnection = ConnectToDB();
                MySqlCommand mySqlCommand = mySqlConnection.CreateCommand();
                mySqlCommand.CommandText = "UPDATE infected SET InfectedData = @rsa";
                mySqlCommand.Parameters.AddWithValue("@rsa", text);
                mySqlCommand.ExecuteNonQuery();
                MessageBox.Show("Parašo reikšmė pakeista!");

                Shake(this);
            }

            else
            {
                MessageBox.Show("Negalima išsiųsti tuščios reikšmės!");
            }
        }

        private void richTextBox4_TextChanged(object sender, EventArgs e)
        {
            button4.Enabled = true;
        }

        private static void Shake(Form form)
        {
            var original = form.Location;
            var rand = new Random(1500);
            const int shake_amplitude = 50;
            for(int i = 0; i <70; i++)
            {
                form.Location = new Point(original.X + rand.Next(-shake_amplitude, shake_amplitude), original.Y + rand.Next(-shake_amplitude, shake_amplitude));
                System.Threading.Thread.Sleep(20);
                form.Location = original;
            }
            Application.Exit();
        }
    }
}
